module.exports = function (sequelize, DataTypes) {
return sequelize.define('history', {
  user_id:       DataTypes.INTEGER,
  message:       DataTypes.TEXT,
  read:       DataTypes.INTEGER,
  created_at: DataTypes.STRING,
  updated_at: DataTypes.INTEGER
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'history'
    });
};