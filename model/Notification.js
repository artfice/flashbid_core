module.exports = function (sequelize, DataTypes) {
return sequelize.define('notification', {
  user_id:       DataTypes.INTEGER,
  message:       DataTypes.TEXT,
  created_at: DataTypes.STRING,
  updated_at: DataTypes.INTEGER
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'history'
    });
};