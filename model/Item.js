module.exports = function (sequelize, DataTypes) {
  return sequelize.define('items', {
  title:       DataTypes.STRING,
  description:       DataTypes.TEXT,
  image:       DataTypes.STRING,
  secondary_image:       DataTypes.STRING,
  type:       DataTypes.ENUM('a', 'l'),
  initial_bid:       DataTypes.INTEGER,
  item_worth:       DataTypes.INTEGER,
  final_bid:       DataTypes.INTEGER,
  bid_amount:       DataTypes.INTEGER,
  buy_at_amount:       DataTypes.INTEGER,
  ticket_amount:       DataTypes.INTEGER,
  ticket_total: DataTypes.INTEGER,
  ticket_sold: DataTypes.INTEGER,
  winner_id: DataTypes.INTEGER,
  room_id: DataTypes.INTEGER, 
  winner_code:       DataTypes.STRING,
  winner_instructions:       DataTypes.STRING,
  status:       DataTypes.ENUM('Lost','Draft', 'Active', 'Flash', 'Complete', 'Claimed', 'Unlisted'),
  created_at: DataTypes.DATE,
  updated_at: DataTypes.DATE,
  size: DataTypes.ENUM('Big', 'Small'),
  vendor_id: DataTypes.INTEGER
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'items'
});
};