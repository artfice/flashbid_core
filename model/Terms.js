module.exports = function (sequelize, DataTypes) {
    return sequelize.define('terms', {
        description: DataTypes.TEXT
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'terms'
    });
};