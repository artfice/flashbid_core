module.exports = function (sequelize, DataTypes) {
return sequelize.define('users', {
  first_name:       DataTypes.STRING,
  last_name:       DataTypes.STRING,
  group_name:       DataTypes.STRING,
  email:       {type: DataTypes.STRING, unique: true},
  password:       DataTypes.STRING,
  profile_image:       DataTypes.STRING,
  phone_number:       DataTypes.INTEGER,
  balance:       DataTypes.INTEGER,
  term_condition:       DataTypes.INTEGER,
  status:       DataTypes.ENUM('Pending', 'Active', 'Suspend', 'Ban'),
  role:       DataTypes.ENUM('Member', 'Admin', 'System', 'Vendor', 'Distributor'),
  access_token: DataTypes.STRING,
  refresh_token: DataTypes.STRING,
  reset_token: DataTypes.STRING,
  confirm_token: DataTypes.STRING,
  bid_attempts: DataTypes.INTEGER,
  money_owed: DataTypes.INTEGER,
  created_at: DataTypes.DATE,
  update_at: DataTypes.DATE,
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'users'
    });
};