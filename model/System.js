module.exports = function (sequelize, DataTypes) {
return sequelize.define('system', {
  num_bid_card: DataTypes.INTEGER,
  num_bid_sold_card: DataTypes.INTEGER,
  value_bid_card: DataTypes.INTEGER,
  value_bid_sold_card: DataTypes.INTEGER,
  bonus_bid_card: DataTypes.INTEGER,
  bonus_bid_sold_card: DataTypes.INTEGER,
  money_owed: DataTypes.INTEGER,
  money_claimed: DataTypes.INTEGER,
  refunds: DataTypes.INTEGER,
  earned: DataTypes.INTEGER
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'system'
    });
};







 