module.exports = function (sequelize, DataTypes) {
return sequelize.define('item_bid', {
  user_id:       DataTypes.INTEGER,
  room_id:       DataTypes.INTEGER,
  item_id:       DataTypes.INTEGER,
  amount:       DataTypes.INTEGER
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'item_bid'
});
};