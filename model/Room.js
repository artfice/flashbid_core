module.exports = function (sequelize, DataTypes) {
return sequelize.define('rooms', {
  start_timestamp: DataTypes.INTEGER,
  flash_timestamp: DataTypes.INTEGER,
  end_timestamp: DataTypes.INTEGER,
  item_id: DataTypes.INTEGER,
  item_title: DataTypes.TEXT,
  item_image: DataTypes.TEXT,  
  initial_bid: DataTypes.INTEGER,
  amount: DataTypes.INTEGER,
  bid_amount: DataTypes.INTEGER,
  buy_at_amount: DataTypes.INTEGER,
  last_bid_name: DataTypes.STRING,
  winner_id: DataTypes.INTEGER,
  display_photo: DataTypes.STRING,
  display_name: DataTypes.STRING,
  state: DataTypes.ENUM('Pending','Open','Flash','Close') 
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'rooms'
});
};