module.exports = function (sequelize, DataTypes) {
return sequelize.define('transactions', {
  action:       DataTypes.STRING,
  user_id:       DataTypes.INTEGER,
  item_id:       DataTypes.INTEGER,
  amount:       DataTypes.INTEGER,
  userOldTotal:       DataTypes.INTEGER,
  userTotal:       DataTypes.INTEGER,
  systemOldTotal:       DataTypes.INTEGER,
  systemTotal:       DataTypes.INTEGER,
  code:       DataTypes.STRING,
  notes:       DataTypes.STRING,
  created_at: DataTypes.DATEONLY,
  updated_at: DataTypes.DATE,
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'transactions'
    });
};