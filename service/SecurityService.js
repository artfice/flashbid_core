var jwt = require('jsonwebtoken');
// var bcrypt = require('bcryptjs');
var sha256 = require('js-sha256');

function SecurityService() {
    return this;
}

SecurityService.prototype.hash = function(text) {
    // return bcrypt.hashSync(text, 1);
    return sha256(text);

};

SecurityService.prototype.compareHash = function (oldPassword, newPassword) {
    // return bcrypt.compareSync(newPassword, oldPassword);
    return oldPassword == sha256(newPassword);
};

SecurityService.prototype.createAccessToken = function (payload, expireIn) {
    // var cert = fs.readFileSync('private.key');
    const secret = process.env.ACCESS_AUTHENTICATION_SECRET;
    const expireTime = expireIn ? expireIn : process.env.ACCESS_TOKEN_EXPIRE_SECOND;
    return jwt.sign(payload, secret, {
          expiresIn: Number(expireTime),
          algorithm: 'HS256'
        });
};

SecurityService.prototype.createRefreshToken = function (payload) {
    // var cert = fs.readFileSync('private.key');
    return jwt.sign(payload, process.env.REFRESH_AUTHENTICATION_SECRET, {
          expiresIn: Number(process.env.REFESH_TOKEN_EXPIRE_SECOND),
          algorithm: 'HS256'
        });
};

SecurityService.prototype.verifyAccessToken = function (token) {
    try {
        return jwt.verify(token, process.env.ACCESS_AUTHENTICATION_SECRET);
    } catch(err) {
        console.log('Verify Access Token Error', err);
        return false;
    }    
};

SecurityService.prototype.verifyRefreshToken = function (token) {
    try {
        return jwt.verify(token, process.env.REFRESH_AUTHENTICATION_SECRET);
    } catch(err) {
        console.log('Verify Refresh Token Error', err);
        return false;
    }    
};

SecurityService.prototype.generateString = function (length) {
    return Math.random().toString(36).substring(2, length) + Math.random().toString(36).substring(2, length);
};

SecurityService.prototype.firebaseUserObject = function (updatedUser) {
    var user = updatedUser.toJSON();
    delete user.access_token;
    delete user.refresh_token;
    delete user.confirm_token;
    delete user.reset_token;
    delete user.term_condition;
    delete user.role;
    delete user.password;    
    return user;
};

module.exports = SecurityService;