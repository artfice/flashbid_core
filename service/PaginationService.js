function PaginationService(page, numItems) {
    this.page = page ? Number(page) : 1;
    this.numItems = numItems ? Number(numItems) : 20;
    this.data = [];
    this.count = 0;
    this.numPages = 0;
    return this;
}

PaginationService.prototype.getItems = function () {
    return this.data;
};

PaginationService.prototype.getPage = function () {
    return this.page;
};

PaginationService.prototype.getCount = function () {
    return this.count;
};

PaginationService.prototype.setCount = function (count) {
    this.count = count;
};

PaginationService.prototype.getNumPages = function () {
    return this.numPages;
};

PaginationService.prototype.setItems = function (data) {
    if (data.length == 1 && !data[0].id) {
        this.count = 0;
        this.numPages = 1;
        return this;
    }
    this.data = data;
    this.numPages = (this.count > 1) ? Math.ceil(this.count / this.numItems) : 1;

    this.data = this.data.map(function(transaction) {
        delete transaction.num;
        return transaction;
    });

    return this;
};

PaginationService.prototype.getOffset = function () {
    return (this.page - 1) * this.numItems
};
PaginationService.prototype.getLimit = function () {
    return this.numItems;
};

module.exports = PaginationService;